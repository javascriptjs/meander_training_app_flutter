import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:meander_training/ForgotPassword.dart';
import 'package:meander_training/Home.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class SignIn extends StatefulWidget {
  @override
  signInPageState createState() => signInPageState();
}

class signInPageState extends State<SignIn> {
  // final scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();
  bool _obscureText = true;
  String validatePass(value) {
    print(value);
    if (value.isEmpty) {
      return "Requried";
    } else {
      return null;
    }
  }

  String validEmail(value) {}
  bool _progress = false;
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  void _login() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _progress = false;
    });
    bool emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(_email.text);
    print(emailValid);
    if (_email.text == '' || _password.text == '') {
      return showAlertDialog(context, 'All Fields Requried');
    }
    if (emailValid == false) {
      return showAlertDialog(context, 'Email Not Valid');
    } else {
      DialogBuilder(context).showLoadingIndicator("");

      Map data = {
        "email_id": _email.text,
        "password": _password.text,
        "app": "string"
      };
      var response = await http.post(
          Uri.parse(
            "https://meander.training/api/login",
          ),
          body: data);
      var loginData = json.decode(response.body);
      // print(loginData.message);
      print(loginData['message']);
      if (loginData['message'] == 'Logged in successfully') {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        prefs.setString('user_id', loginData['user_id']);
        DialogBuilder(context).hideOpenDialog();
        print(loginData);
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => Home()));
        print('response is ????????' + response.body);
      } else {
        DialogBuilder(context).hideOpenDialog();
        // If the server did not return a 200 OK response,
        // then throw an exception.
        print(loginData['message']);
        return showAlertDialog(context, loginData['message']);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "",
          ),
        ),
        body: Padding(
          padding: EdgeInsets.all(0),
          child: Center(
            child: Form(
              autovalidate: true,
              key: formkey,
              child: SizedBox.expand(
                // -> 01
                child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/bg.png"),
                        fit: BoxFit.cover, // -> 02
                      ),
                    ),
                    child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,

                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.fromLTRB(10, 60, 10, 0),

                            // width: 440,
                            child: new Text(
                              'Welcome Back! ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25,
                                  color: Colors.white),
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.fromLTRB(20, 70, 20, 30),
                              height: 70,
                              // width: 200,
                              child: TextFormField(
                                style: TextStyle(color: Colors.white),
                                controller: _email,
                                decoration: InputDecoration(
                                    // border: OutlineInputBorder(
                                    //     borderSide: new BorderSide(
                                    //         color: Colors.red, width: 5.0)),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    labelStyle: TextStyle(
                                      color: Colors.white,
                                    ),
                                    labelText: "Email"),
                                validator: MultiValidator([
                                  RequiredValidator(errorText: "Requried *"),
                                  EmailValidator(errorText: "Not a Valid Email")
                                ]),
                              )),

                          Container(
                              margin: EdgeInsets.fromLTRB(20, 10, 20, 20),
                              height: 70,
                              // width: 200,
                              child: TextFormField(
                                  style: TextStyle(color: Colors.white),
                                  controller: _password,
                                  decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    // hintText: 'username@mail.com',
                                    labelText: 'Password',
                                    // hintStyle: TextStyle(color: Colors.white),
                                    labelStyle: TextStyle(
                                        color: Colors.white, fontSize: 20),
                                    suffixIcon: IconButton(
                                      icon: Icon(
                                        _obscureText
                                            ? Icons.visibility
                                            : Icons.visibility_off,
                                        color: Colors.white,
                                      ),
                                      onPressed: _toggle,
                                    ),
                                  ),
                                  obscureText: _obscureText,
                                  validator: validatePass)),

                          Padding(
                              padding: EdgeInsets.only(right: 20),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ForgotPassword()));
                                    },
                                    child: Text('Forgot Password',
                                        textDirection: TextDirection.ltr,
                                        style: TextStyle(
                                            fontSize: 17,
                                            color: Colors.white,
                                            decoration:
                                                TextDecoration.underline,
                                            fontWeight: FontWeight.w700)),
                                  ),
                                ],
                              )),
                          // Padding(
                          //     padding: EdgeInsets.only(top: 20.0),
                          //     child: RaisedButton(
                          //       // onPressed: validate,
                          //       child: Text("Login"),

                          //     )),

                          InkWell(
                            child: Container(
                              margin: EdgeInsets.fromLTRB(20, 70, 20, 30),
                              height: 55,
                              // width: 200,
                              child: Card(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                        // margin: EdgeInsets.fromLTRB(10, 0, 10, 0),

                                        // width: 440,
                                        ),

                                    // Image.asset("assets/logo.png"),
                                    new Text(
                                      'Login',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                          color: Colors.lightBlue),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            onTap: () {
                              _login();
                            },
                          ),
                        ])),
              ),
            ),
          ),
        ));
  }
}

showAlertDialog(BuildContext context, emailError) {
  // Create button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );

  // Create AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(emailError),
    // content: Text("This is an alert message."),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class DialogBuilder {
  DialogBuilder(this.context);

  final BuildContext context;

  void showLoadingIndicator([String text]) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
            onWillPop: () async => false,
            child: AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0))),
              backgroundColor: Colors.black87,
              content: LoadingIndicator(text: text),
            ));
      },
    );
  }

  void hideOpenDialog() {
    Navigator.of(context).pop();
  }
}

class LoadingIndicator extends StatelessWidget {
  LoadingIndicator({this.text = ''});

  final String text;

  @override
  Widget build(BuildContext context) {
    var displayedText = text;

    return Container(
        padding: EdgeInsets.all(16),
        color: Colors.black87,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              _getLoadingIndicator(),
              _getHeading(context),
              _getText(displayedText)
            ]));
  }

  Padding _getLoadingIndicator() {
    return Padding(
        child: Container(
            child: CircularProgressIndicator(strokeWidth: 3),
            width: 32,
            height: 32),
        padding: EdgeInsets.only(bottom: 16));
  }

  Widget _getHeading(context) {
    return Padding(
        child: Text(
          'Please wait …',
          style: TextStyle(color: Colors.white, fontSize: 16),
          textAlign: TextAlign.center,
        ),
        padding: EdgeInsets.only(bottom: 4));
  }

  Text _getText(String displayedText) {
    return Text(
      displayedText,
      style: TextStyle(color: Colors.white, fontSize: 14),
      textAlign: TextAlign.center,
    );
  }
}
