// import 'package:flutter/material.dart';
// import 'package:meander_training/HomeListing.dart';

// class Home extends StatefulWidget {
//   @override
//   HomePageState createState() => HomePageState();
// }

// class HomePageState extends State<Home> {
//   // final scaffoldKey = GlobalKey<ScaffoldState>();
//   int _currentIndex = 0;
//   final List<Widget> _children = [
//     HomeListing(),
//     HomeListing(),
//     HomeListing(),
//     HomeListing()
//   ];
//   void onTabTapped(int index) {
//     setState(() {
//       _currentIndex = index;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(
//           "Meander Training",
//         ),
//       ),
//       body: _children[_currentIndex],
//       // bottomNavigationBar: BottomNavigationBar(
//       //   onTap: onTabTapped, // new
//       //   currentIndex: _currentIndex, // new
//       //   items: [
//       //     new BottomNavigationBarItem(
//       //       icon: Icon(Icons.home),
//       //       title: Text('Home'),
//       //     ),
//       //     new BottomNavigationBarItem(
//       //       icon: Icon(Icons.library_books),
//       //       title: Text('My Course'),
//       //     ),
//       //     new BottomNavigationBarItem(
//       //         icon: Icon(Icons.person), title: Text('Profile')),
//       //     new BottomNavigationBarItem(
//       //       icon: Icon(Icons.library_books),
//       //       title: Text('WishList'),
//       //     ),
//       //   ],
//       // ),
//       bottomNavigationBar: new TabBar(
//         tabs: [
//           Tab(
//             icon: new Icon(Icons.home),
//           ),
//           Tab(
//             icon: new Icon(Icons.rss_feed),
//           ),
//           Tab(
//             icon: new Icon(Icons.perm_identity),
//           ),
//           Tab(
//             icon: new Icon(Icons.settings),
//           )
//         ],
//         labelColor: Colors.yellow,
//         unselectedLabelColor: Colors.blue,
//         indicatorSize: TabBarIndicatorSize.label,
//         indicatorPadding: EdgeInsets.all(5.0),
//         indicatorColor: Colors.red,
//       ),
//     );
//   }
// }

import 'package:flutter/material.dart';
import 'package:meander_training/Account.dart';
import 'package:meander_training/HomeListing.dart';
import 'package:meander_training/MyCourse.dart';
import 'package:meander_training/WishList.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new MaterialApp(
      color: Colors.yellow,
      home: DefaultTabController(
        length: 4,
        child: new Scaffold(
          body: TabBarView(
            children: [
              HomeListing(),
              MyCourse(),
              WishList(),
              Account(),
            ],
          ),
          bottomNavigationBar: new TabBar(
            tabs: [
              Tab(
                text: 'Home',
                icon: new Icon(Icons.home, size: 30.0),
              ),
              Tab(
                text: 'My Course',
                icon: new Icon(Icons.library_books_outlined, size: 30.0),
              ),
              Tab(
                text: 'WishList',
                icon: new Icon(Icons.favorite, size: 30.0),
              ),
              Tab(
                text: 'Account',
                icon: new Icon(Icons.account_circle_rounded, size: 30.0),
              )
            ],
            labelColor: Colors.blueAccent,
            unselectedLabelColor: Colors.grey,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorPadding: EdgeInsets.all(5.0),
            indicatorColor: Colors.blueAccent,
          ),
          backgroundColor: Colors.white,
        ),
      ),
    );
  }
}
