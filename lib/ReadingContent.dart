import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class ReadingContent extends StatefulWidget {
  @override
  ReadingContentPageState createState() => ReadingContentPageState();
}

class ReadingContentPageState extends State<ReadingContent> {
  // final scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  TextEditingController _password = TextEditingController();

  void _login() {}
  String validatePass(value) {
    print(value);
    if (value.isEmpty) {
      return "Requried";
    } else if (value.length < 4) {
      return "should Be atleast 4 charcaters";
    } else {
      return null;
    }
  }

  var HtmlCode = '<h1> h1 Heading Tag</h1>' +
      '<h2> h2 Heading Tag </h2>' +
      '<p> Sample Paragraph Tag </p>' +
      '<img src="https://flutter-examples.com/wp-content/uploads/2019/04/install_thumb.png" alt="Image" width="250" height="150" border="3">';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset("assets/logo.png",
                width: 90, height: 50, fit: BoxFit.fill),
            Text(
              "Meander Training",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color: Colors.black),
            ),
          ],
        ),
        backgroundColor: Colors.white,
      ),
      body: WebviewScaffold(
        url: new Uri.dataFromString(HtmlCode, mimeType: 'text/html').toString(),
      ),
    );
  }
}
