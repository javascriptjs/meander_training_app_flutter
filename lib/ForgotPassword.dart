import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

class ForgotPassword extends StatefulWidget {
  @override
  forgotPasswordPageState createState() => forgotPasswordPageState();
}

class forgotPasswordPageState extends State<ForgotPassword> {
  // final scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  TextEditingController _email = TextEditingController();

  void _login() {
    print(_email.text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "",
          ),
        ),
        body: Padding(
          padding: EdgeInsets.all(0),
          child: Center(
            child: Form(
              autovalidate: true,
              key: formkey,
              child: SizedBox.expand(
                // -> 01
                child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/bg.png"),
                        fit: BoxFit.cover, // -> 02
                      ),
                    ),
                    child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,

                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.fromLTRB(10, 100, 10, 0),

                            // width: 440,
                            child: new Text(
                              'Forgot Password',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25,
                                  color: Colors.white),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(10, 10, 10, 0),

                            // width: 440,
                            child: new Text(
                              'Enter your registered e-mail adress to\nget reset password link',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14,
                                  color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.fromLTRB(20, 70, 20, 30),
                              height: 70,
                              // width: 200,
                              child: TextFormField(
                                style: TextStyle(color: Colors.white),
                                controller: _email,
                                decoration: InputDecoration(
                                    // border: OutlineInputBorder(
                                    //     borderSide: new BorderSide(
                                    //         color: Colors.red, width: 5.0)),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    labelStyle: TextStyle(
                                      color: Colors.white,
                                    ),
                                    labelText: "Email"),
                                validator: MultiValidator([
                                  RequiredValidator(errorText: "Requried *"),
                                  EmailValidator(errorText: "Not a Valid Email")
                                ]),
                              )),
                          InkWell(
                            child: Container(
                              margin: EdgeInsets.fromLTRB(20, 70, 20, 30),
                              height: 55,
                              // width: 200,
                              child: Card(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                        // margin: EdgeInsets.fromLTRB(10, 0, 10, 0),

                                        // width: 440,
                                        ),

                                    // Image.asset("assets/logo.png"),
                                    new Text(
                                      'Submit',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                          color: Colors.lightBlue),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            onTap: () {
                              // Navigator.of(context).push(MaterialPageRoute(
                              //     builder: (context) => SignIn()));
                              _login();
                            },
                          ),
                        ])),
              ),
            ),
          ),
        ));
  }
}
