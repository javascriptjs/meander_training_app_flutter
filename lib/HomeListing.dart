import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:meander_training/CaricullumScreen.dart';
import 'package:meander_training/SeeAll.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class HomeListing extends StatefulWidget {
  @override
  homeListingPageState createState() => homeListingPageState();
}

// final List<String> entries = <String>['A', 'B', 'C'];
final List<int> colorCodes = <int>[600, 500, 100];
int ArrayLength = 3;
List users = [];

class homeListingPageState extends State<HomeListing> {
  List<String> listModel = [];
  @override
  void initState() {
    super.initState();
    getList();
  }

  // final scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  TextEditingController _email = TextEditingController();
  void getList() async {
    var response = await http.get(
      Uri.parse(
        "https://meander.training/api/homelisting",
      ),
    );
    var loginData = json.decode(response.body);
    setState(() {
      users = loginData['courses'];
    });
  }

  @override
  Widget build(BuildContext context) {
    print(users);
    return Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset("assets/logo.png",
                  width: 90, height: 50, fit: BoxFit.fill),
              Text(
                "Meander Training",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    color: Colors.black),
              ),
            ],
          ),
          backgroundColor: Colors.white,
        ),
        body: Padding(
            padding: EdgeInsets.all(0),
            // child: Center(

            // -> 01

            child: Padding(
              padding: EdgeInsets.all(0),
              child: SingleChildScrollView(
                  // mainAxisAlignment: MainAxisAlignment.start,
                  child: Column(children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.fromLTRB(10, 10, 00, 0),
                      child: new Text(
                        'Top Courses ',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: Colors.black),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.fromLTRB(00, 10, 10, 0),
                      child: InkWell(
                        onTap: () {
                          // Navigator.of(context).push(MaterialPageRoute(
                          //     builder: (context) => SeeAll()));
                          //
                          getList();
                        },
                        child: Text(
                          'See All',
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  ],
                ),
                // course card/////////////////
                // ListView.builder(
                //     padding: const EdgeInsets.all(8),
                //     itemCount: users.length,
                //     scrollDirection: Axis.vertical,
                //     shrinkWrap: true,
                //     itemBuilder: (BuildContext context, int index) {
                Container(
                  margin: EdgeInsets.fromLTRB(10, 30, 00, 0),
                  height: 200.0,
                  child: ListView.builder(
                      padding: const EdgeInsets.all(8),
                      itemCount: users.length,
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          // height: 150,
                          margin: EdgeInsets.fromLTRB(00, 00, 30, 0),
                          //
                          width: 200,
                          color: Colors.white,
                          child: Column(
                            // mainAxisAlignment: MainAxisAlignment.center,
                            // crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image.network("${users[index]['course_logo']}",
                                  width: 200, height: 120, fit: BoxFit.fill),
                              Container(
                                alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(00, 10, 00, 0),
                                // width: 100,
                                child: Text(
                                  ' ${users[index]['course_name']}',
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                              )
                            ],
                          ),

                          //  Center(
                          //     child: Text('Entry ${users[index]['course_id']}')),
                        );
                      }),
                ),
                // }),

                ///////course card endd
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.fromLTRB(10, 10, 00, 0),
                      child: new Text(
                        'Student are vewing ',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: Colors.black),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.fromLTRB(00, 10, 10, 0),
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => SeeAll()));
                        },
                        child: Text(
                          'See All',
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  ],
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(10, 30, 00, 0),
                  height: 200.0,
                  child: ListView.builder(
                      padding: const EdgeInsets.all(8),
                      itemCount: users.length,
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          // height: 150,
                          margin: EdgeInsets.fromLTRB(00, 00, 30, 0),
                          //
                          width: 200,
                          color: Colors.white,
                          child: Column(
                            // mainAxisAlignment: MainAxisAlignment.center,
                            // crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image.network("${users[index]['course_logo']}",
                                  width: 200, height: 120, fit: BoxFit.fill),
                              Container(
                                alignment: Alignment.center,
                                margin: EdgeInsets.fromLTRB(00, 10, 00, 0),
                                // width: 100,
                                child: Text(
                                  ' ${users[index]['course_name']}',
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                              )
                            ],
                          ),

                          //  Center(
                          //     child: Text('Entry ${users[index]['course_id']}')),
                        );
                      }),
                ),
              ])),
            ))
        // ),
        );
  }
}
