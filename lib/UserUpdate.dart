import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:meander_training/ChangePassword.dart';
import 'package:meander_training/UserUpdate.dart';

class UserUpdate extends StatefulWidget {
  @override
  UserUpdatePageState createState() => UserUpdatePageState();
}

class UserUpdatePageState extends State<UserUpdate> {
  // final scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  TextEditingController _password = TextEditingController();

  void _login() {}
  String validatePass(value) {
    print(value);
    if (value.isEmpty) {
      return "Requried";
    } else if (value.length < 4) {
      return "should Be atleast 4 charcaters";
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            // mainAxisAlignment: MainAxisAlignment.center,
            // crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset("assets/logo.png",
                  width: 90, height: 50, fit: BoxFit.fill),
              Text(
                "Meander Training",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    color: Colors.black),
              ),
            ],
          ),
          backgroundColor: Colors.white,
        ),
        body: Padding(
          padding: EdgeInsets.all(0),
          child: Center(
            child: Form(
              autovalidate: true,
              key: formkey,
              child: SizedBox.expand(
                // -> 01
                child: SingleChildScrollView(
                    child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,

                        children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(15),
                        child: TextField(
                          decoration: InputDecoration(
                            border: UnderlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0)),
                            labelText: 'User Name',
                            hintText: 'User Name',
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(15),
                        child: TextField(
                          decoration: InputDecoration(
                            border: UnderlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0)),
                            labelText: 'Email',
                            hintText: 'Email',
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(15),
                        child: TextField(
                            decoration: InputDecoration(
                              border: UnderlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0)),
                              labelText: 'Phone Number',
                              hintText: 'Phone Number',
                            ),
                            keyboardType: TextInputType.number),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(20, 40, 20, 0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            // BorderRadius.all(Radius.circular(3)),
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey, width: 1.0))),
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => ChangePassword()));
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            // crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                margin: EdgeInsets.fromLTRB(00, 00, 00, 20),
                                alignment: Alignment.center,
                                child: new Text(
                                  'Change Password',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                      color: Colors.grey),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(00, 00, 00, 20),
                                child: Icon(
                                  Icons.arrow_forward_ios_outlined,
                                  color: Colors.grey,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        child: Container(
                          margin: EdgeInsets.fromLTRB(20, 60, 20, 30),
                          height: 55,
                          // width: 200,
                          child: Card(
                            color: Colors.blueAccent,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                    // margin: EdgeInsets.fromLTRB(10, 0, 10, 0),

                                    // width: 440,
                                    ),

                                // Image.asset("assets/logo.png"),
                                new Text(
                                  'Submit',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                      color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        ),
                        onTap: () {
                          // _register();
                        },
                      ),
                    ])),
              ),
            ),
          ),
        ));
  }
}
