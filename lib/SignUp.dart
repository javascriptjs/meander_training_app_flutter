import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:meander_training/SignIn.dart';

class SignUp extends StatefulWidget {
  @override
  signUpPageState createState() => signUpPageState();
}

class signUpPageState extends State<SignUp> {
  // final scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  TextEditingController _email = TextEditingController();
  TextEditingController _firstName = TextEditingController();
  TextEditingController _lastName = TextEditingController();
  TextEditingController _reEmail = TextEditingController();

  TextEditingController _password = TextEditingController();
  bool _obscureText = true;
  String validatePass(value) {
    if (value.isEmpty) {
      return "Requried";
    } else if (value.length < 4) {
      return "should Be atleast 4 charcaters";
    } else {
      return null;
    }
  }

  void _register() async {
    print(_firstName.text);
    // print(_reEmail);
    var emailError = 'Email Not Match';
    bool emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(_email.text);
    if (_firstName.text == '' ||
        _lastName.text == '' ||
        _email.text == '' ||
        _reEmail.text == '' ||
        _password.text == '') {
      return showAlertDialog(context, 'All Fields Requried');
    } else if (emailValid == false) {
      return showAlertDialog(context, 'Email Not Valid');
    } else if (_email.text != _reEmail.text) {
      return showAlertDialog(context, emailError);
    } else {
      DialogBuilder(context).showLoadingIndicator("");

      Map data = {
        "email_id": _email.text,
        "password": _password.text,
        "app": "string"
      };
      var response = await http.post(
          Uri.parse(
            "https://meander.training/api/login",
          ),
          body: data);
      var loginData = json.decode(response.body);
      // print(loginData.message);
      print(loginData['message']);
      if (loginData['message'] == 'Logged in successfully') {
        // If the server did return a 200 OK response,
        // then parse the JSON.
        DialogBuilder(context).hideOpenDialog();
        print(data);
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => SignIn()));
        print('response is ????????' + response.body);
      } else {
        DialogBuilder(context).hideOpenDialog();
        // If the server did not return a 200 OK response,
        // then throw an exception.
        print(loginData['message']);
        return showAlertDialog(context, loginData['message']);
      }
    }
  }

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "",
          ),
        ),
        body: Padding(
          padding: EdgeInsets.all(0),
          child: Center(
            child: Form(
              autovalidate: true,
              key: formkey,
              child: SizedBox.expand(
                  // -> 01
                  child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("assets/bg.png"),
                          fit: BoxFit.cover, // -> 02
                        ),
                      ),
                      child: SingleChildScrollView(
                          // mainAxisAlignment: MainAxisAlignment.center,
                          child: Column(children: <Widget>[
                        Container(
                          margin: EdgeInsets.fromLTRB(10, 30, 10, 0),

                          // width: 440,
                          child: new Text(
                            'Create an account ',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 25,
                                color: Colors.white),
                          ),
                        ),
                        Container(
                            margin: EdgeInsets.fromLTRB(20, 70, 20, 00),
                            height: 70,
                            // width: 200,
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              controller: _firstName,
                              decoration: InputDecoration(
                                  // border: OutlineInputBorder(
                                  //     borderSide: new BorderSide(
                                  //         color: Colors.red, width: 5.0)),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.white, width: 2.0),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.white, width: 2.0),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.white, width: 2.0),
                                  ),
                                  labelStyle: TextStyle(
                                    color: Colors.white,
                                  ),
                                  labelText: "First Name"),
                              validator: MultiValidator([
                                RequiredValidator(errorText: "Requried *"),
                              ]),
                            )),

                        Container(
                            margin: EdgeInsets.fromLTRB(20, 10, 20, 00),
                            height: 70,
                            // width: 200,
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              controller: _lastName,
                              decoration: InputDecoration(
                                  // border: OutlineInputBorder(
                                  //     borderSide: new BorderSide(
                                  //         color: Colors.red, width: 5.0)),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.white, width: 2.0),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.white, width: 2.0),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.white, width: 2.0),
                                  ),
                                  labelStyle: TextStyle(
                                    color: Colors.white,
                                  ),
                                  labelText: "Last Name"),
                              validator: MultiValidator([
                                RequiredValidator(errorText: "Requried *"),
                              ]),
                            )),

                        Container(
                            margin: EdgeInsets.fromLTRB(20, 20, 20, 00),
                            height: 70,
                            // width: 200,
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              controller: _email,
                              decoration: InputDecoration(
                                  // border: OutlineInputBorder(
                                  //     borderSide: new BorderSide(
                                  //         color: Colors.red, width: 5.0)),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.white, width: 2.0),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.white, width: 2.0),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.white, width: 2.0),
                                  ),
                                  labelStyle: TextStyle(
                                    color: Colors.white,
                                  ),
                                  labelText: "Email"),
                              validator: MultiValidator([
                                RequiredValidator(errorText: "Requried *"),
                                EmailValidator(errorText: "Not a Valid Email")
                              ]),
                            )),

                        Container(
                            margin: EdgeInsets.fromLTRB(20, 20, 20, 00),
                            height: 70,
                            // width: 200,
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              controller: _reEmail,
                              decoration: InputDecoration(
                                  // border: OutlineInputBorder(
                                  //     borderSide: new BorderSide(
                                  //         color: Colors.red, width: 5.0)),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.white, width: 2.0),
                                  ),
                                  disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.white, width: 2.0),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.white, width: 2.0),
                                  ),
                                  labelStyle: TextStyle(
                                    color: Colors.white,
                                  ),
                                  labelText: "Re Enter Email"),
                              validator: MultiValidator([
                                RequiredValidator(errorText: "Requried *"),
                                EmailValidator(errorText: "Not a Valid Email")
                              ]),
                            )),

                        Container(
                            margin: EdgeInsets.fromLTRB(20, 10, 20, 20),
                            height: 70,
                            // width: 200,
                            child: TextFormField(
                                style: TextStyle(color: Colors.white),
                                controller: _password,
                                decoration: InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.white, width: 2.0),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.white, width: 2.0),
                                  ),
                                  // hintText: 'username@mail.com',
                                  labelText: 'Password',
                                  // hintStyle: TextStyle(color: Colors.white),
                                  labelStyle: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                  suffixIcon: IconButton(
                                    icon: Icon(
                                      _obscureText
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      color: Colors.white,
                                    ),
                                    onPressed: _toggle,
                                  ),
                                ),
                                obscureText: _obscureText,
                                validator: validatePass)),

                        // Padding(
                        //     padding: EdgeInsets.only(top: 20.0),
                        //     child: RaisedButton(
                        //       // onPressed: validate,
                        //       child: Text("Login"),

                        //     )),

                        InkWell(
                          child: Container(
                            margin: EdgeInsets.fromLTRB(20, 20, 20, 30),
                            height: 55,
                            // width: 200,
                            child: Card(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                      // margin: EdgeInsets.fromLTRB(10, 0, 10, 0),

                                      // width: 440,
                                      ),

                                  // Image.asset("assets/logo.png"),
                                  new Text(
                                    'Register',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        color: Colors.lightBlue),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          onTap: () {
                            _register();
                            // Navigator.of(context).push(MaterialPageRoute(
                            //     builder: (context) => SignIn()));
                          },
                        ),
                      ])))),
            ),
          ),
        ));
  }
}

showAlertDialog(BuildContext context, emailError) {
  // Create button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );

  // Create AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(emailError),
    // content: Text("This is an alert message."),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class DialogBuilder {
  DialogBuilder(this.context);

  final BuildContext context;

  void showLoadingIndicator([String text]) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
            onWillPop: () async => false,
            child: AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0))),
              backgroundColor: Colors.black87,
              content: LoadingIndicator(text: text),
            ));
      },
    );
  }

  void hideOpenDialog() {
    Navigator.of(context).pop();
  }
}

class LoadingIndicator extends StatelessWidget {
  LoadingIndicator({this.text = ''});

  final String text;

  @override
  Widget build(BuildContext context) {
    var displayedText = text;

    return Container(
        padding: EdgeInsets.all(16),
        color: Colors.black87,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              _getLoadingIndicator(),
              _getHeading(context),
              _getText(displayedText)
            ]));
  }

  Padding _getLoadingIndicator() {
    return Padding(
        child: Container(
            child: CircularProgressIndicator(strokeWidth: 3),
            width: 32,
            height: 32),
        padding: EdgeInsets.only(bottom: 16));
  }

  Widget _getHeading(context) {
    return Padding(
        child: Text(
          'Please wait …',
          style: TextStyle(color: Colors.white, fontSize: 16),
          textAlign: TextAlign.center,
        ),
        padding: EdgeInsets.only(bottom: 4));
  }

  Text _getText(String displayedText) {
    return Text(
      displayedText,
      style: TextStyle(color: Colors.white, fontSize: 14),
      textAlign: TextAlign.center,
    );
  }
}
