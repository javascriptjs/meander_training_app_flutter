import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:meander_training/UserUpdate.dart';

class Account extends StatefulWidget {
  @override
  AccountPageState createState() => AccountPageState();
}

class AccountPageState extends State<Account> {
  // final scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  TextEditingController _password = TextEditingController();

  void _login() {}
  String validatePass(value) {
    print(value);
    if (value.isEmpty) {
      return "Requried";
    } else if (value.length < 4) {
      return "should Be atleast 4 charcaters";
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset("assets/logo.png",
                  width: 90, height: 50, fit: BoxFit.fill),
              Text(
                "Meander Training",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    color: Colors.black),
              ),
            ],
          ),
          backgroundColor: Colors.white,
        ),
        body: Padding(
          padding: EdgeInsets.all(0),
          child: Center(
            child: Form(
              autovalidate: true,
              key: formkey,
              child: SizedBox.expand(
                // -> 01
                child: SingleChildScrollView(
                    child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,

                        children: <Widget>[
                      Container(
                        margin: EdgeInsets.fromLTRB(20, 40, 20, 0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            // BorderRadius.all(Radius.circular(3)),
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey, width: 1.0))),
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => UserUpdate()));
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            // crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                margin: EdgeInsets.fromLTRB(00, 00, 00, 20),
                                alignment: Alignment.center,
                                child: new Text(
                                  'Account Setting',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                      color: Colors.black),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(00, 00, 00, 20),
                                child: Icon(
                                  Icons.arrow_forward_ios_outlined,
                                  color: Colors.black,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(20, 40, 20, 0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            // BorderRadius.all(Radius.circular(3)),
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey, width: 1.0))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          // crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(00, 00, 00, 20),
                              alignment: Alignment.center,
                              child: new Text(
                                'About us',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    color: Colors.black),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(00, 00, 00, 20),
                              child: Icon(
                                Icons.arrow_forward_ios_outlined,
                                color: Colors.black,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(20, 40, 20, 0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            // BorderRadius.all(Radius.circular(3)),
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey, width: 1.0))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          // crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(00, 00, 00, 20),
                              alignment: Alignment.center,
                              child: new Text(
                                'Privacy Policy',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    color: Colors.black),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(00, 00, 00, 20),
                              child: Icon(
                                Icons.arrow_forward_ios_outlined,
                                color: Colors.black,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(20, 40, 20, 0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            // BorderRadius.all(Radius.circular(3)),
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey, width: 1.0))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          // crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(00, 00, 00, 20),
                              alignment: Alignment.center,
                              child: new Text(
                                'Contact us',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    color: Colors.black),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(00, 00, 00, 20),
                              child: Icon(
                                Icons.arrow_forward_ios_outlined,
                                color: Colors.black,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(00, 100, 00, 20),
                        alignment: Alignment.center,
                        child: new Text(
                          'Logout',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 25,
                              color: Colors.redAccent),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ])),
              ),
            ),
          ),
        ));
  }
}
