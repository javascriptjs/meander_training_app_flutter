import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class CaricullumScreen extends StatefulWidget {
  final String text;
  // final String textCreator;
  // final String courseName;
  // final String course_Logo;
  // final String over_view;

  CaricullumScreen({
    Key key,
    @required this.text,
  }) : super(key: key);
  @override
  CaricullumScreenPageState createState() => CaricullumScreenPageState();
}

class CaricullumScreenPageState extends State<CaricullumScreen> {
  @override
  void initState() {
    super.initState();
    showSelectedCourse();
  }

  // final scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  TextEditingController _password = TextEditingController();
  String course_name = '';
  String creator_text = '';
  String course_id = '';
  String overView = '';
  int contentLength = 0;
  int chapterLength = 0;
  var courseLogo;
  var _likeStatus = false;
  void addWishList() async {
    // setState(() {
    //   _likeStatus = !_likeStatus;
    // });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('user_id');
    Map data = {
      "course_id": course_id,
      "user_id": token,
    };
    //
    var response = await http.post(
        Uri.parse(
          "https://meander.training/api/wishlist",
        ),
        body: data);
    var loginData = json.decode(response.body);
  }

  void showSelectedCourse() async {
    // var id = widget.text;
    setState(() {
      course_id = widget.text;
    });
    // print(id);
    // var coursesLogo = widget.course_Logo.toString();
    // String newMessage = json.encode(message);

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('user_id');
    Map data = {
      "course_id": course_id,
      "user_id": token,
    };
    //
    var response = await http.post(
        Uri.parse(
          "https://meander.training/api/getCourse",
        ),
        body: data);
    var loginData = json.decode(response.body);
    print(loginData);
    List Chapters = [];
    var TopicData;
    List contet = (loginData['data']['content']);
    contet.forEach((age) => Chapters = age['chapters']);
    // Chapters.forEach((item) => (print(item)));

    setState(() {
      contentLength = contet.length;
      chapterLength = Chapters.length;
      course_name = loginData['data']['course_name'];
      overView = loginData['data']['overview'];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            // mainAxisAlignment: MainAxisAlignment.center,
            // crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset("assets/logo.png",
                  width: 90, height: 50, fit: BoxFit.fill),
              Text(
                "Meander Training",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    color: Colors.black),
              ),
            ],
          ),
          backgroundColor: Colors.white,
        ),
        body: Padding(
          padding: EdgeInsets.all(0),
          // child: Center(
          child: Form(
            autovalidate: true,
            key: formkey,

            // -> 01
            child: SingleChildScrollView(
                child: Column(
                    // mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                  new Stack(
                    children: <Widget>[
                      // The containers in the background
                      new Column(
                        children: <Widget>[
                          new Container(
                            height: MediaQuery.of(context).size.height * .30,
                            color: Colors.blue,
                            child: InkWell(
                              child: Container(
                                margin: EdgeInsets.fromLTRB(00, 00, 00, 00),
                                height: 200,
                                // width: 200,
                                child: Card(
                                  color: Colors.blueAccent,
                                  child: Row(
                                    // mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.fromLTRB(
                                                10, 10, 00, 0),
                                            child: new Text(
                                              course_name,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 17,
                                                  color: Colors.white),
                                            ),
                                            // width: 440,
                                          ),
                                          Container(
                                            margin: EdgeInsets.fromLTRB(
                                                10, 10, 00, 0),
                                            child: new Text(
                                              creator_text,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 14,
                                                  color: Colors.white),
                                              textAlign: TextAlign.start,
                                            ),
                                            // width: 440,
                                          ),
                                        ],
                                      )

                                      // Image.asset("assets/logo.png"),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          new Container(
                            height: MediaQuery.of(context).size.height * .0,
                            color: Colors.white,
                          )
                        ],
                      ),
                      // The card widget with top padding,
                      // incase if you wanted bottom padding to work,
                      // set the `alignment` of container to Alignment.bottomCenter
                      new Container(
                        alignment: Alignment.topCenter,
                        padding: new EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * .20,
                            right: 20.0,
                            left: 20.0),
                        child: new Container(
                          height: 200.0,
                          width: MediaQuery.of(context).size.width,
                          child: Image.asset("assets/book1.png",
                              width: 300, height: 150, fit: BoxFit.fill),
                        ),
                      )
                    ],
                  ),
                  InkWell(
                    child: Container(
                      margin: EdgeInsets.fromLTRB(00, 20, 00, 00),
                      height: 55,
                      width: 200,
                      child: Card(
                        color: Colors.white,
                        shape: new RoundedRectangleBorder(
                            side: new BorderSide(
                                color: Colors.blueAccent, width: 1.0),
                            borderRadius: BorderRadius.circular(10.0)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(10, 0, 5, 0),

                              // width: 440,

                              child: Icon(
                                Icons.favorite,
                                color: _likeStatus == false
                                    ? Colors.grey
                                    : Colors.redAccent,
                              ),
                            ),

                            // Image.asset("assets/logo.png"),
                            new Text(
                              _likeStatus == false
                                  ? 'Add To WishList'
                                  : "Remove To WishList",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14,
                                  color: Colors.grey),
                            ),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      // _register();
                      addWishList();
                    },
                  ),
                  new Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(color: Colors.white),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                          offset: Offset(
                              2.0, 2.0), // shadow direction: bottom right
                        )
                      ],
                    ),
                    margin: EdgeInsets.fromLTRB(20, 20, 20, 00),
                    padding: new EdgeInsets.only(
                        // top: MediaQuery.of(context).size.height * .20,
                        right: 20.0,
                        left: 20.0),
                    child: new Container(
                        height: 100.0,
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(10, 10, 00, 0),
                              child: new Text(
                                'This course includes:',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14,
                                    color: Colors.black),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                    margin: EdgeInsets.fromLTRB(00, 20, 5, 0),

                                    // width: 440,

                                    child: Column(
                                      children: [
                                        Text(
                                          'Section',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 14,
                                              color: Colors.grey),
                                        ),
                                        Text(
                                          contentLength.toString(),
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 14,
                                              color: Colors.black),
                                        ),
                                      ],
                                    )),

                                // Image.asset("assets/logo.png"),
                                Container(
                                    margin: EdgeInsets.fromLTRB(00, 20, 5, 0),

                                    // width: 440,

                                    child: Column(
                                      children: [
                                        Text(
                                          'Chapters',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 14,
                                              color: Colors.grey),
                                        ),
                                        Text(
                                          chapterLength.toString(),
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 14,
                                              color: Colors.black),
                                        ),
                                      ],
                                    )),
                                Container(
                                    margin: EdgeInsets.fromLTRB(00, 20, 5, 0),

                                    // width: 440,

                                    child: Column(
                                      children: [
                                        Text(
                                          'Topic',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 14,
                                              color: Colors.grey),
                                        ),
                                        Text(
                                          '180',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 14,
                                              color: Colors.black),
                                        ),
                                      ],
                                    )),
                              ],
                            ),
                          ],
                        )),
                  ),

                  // /////////////////over view box//////////////
                  new Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(color: Colors.white),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                          offset: Offset(
                              2.0, 2.0), // shadow direction: bottom right
                        )
                      ],
                    ),
                    margin: EdgeInsets.fromLTRB(20, 20, 20, 00),
                    padding: new EdgeInsets.only(
                        // top: MediaQuery.of(context).size.height * .20,
                        right: 20.0,
                        left: 20.0),
                    child: new Container(
                        height: 100.0,
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(00, 10, 00, 0),
                              child: new Text(
                                'Description :',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14,
                                    color: Colors.black),
                                textAlign: TextAlign.left,
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                    margin: EdgeInsets.fromLTRB(00, 20, 5, 0),

                                    // width: 440,

                                    child: Column(
                                      children: [
                                        Text(
                                          overView,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 14,
                                              color: Colors.grey),
                                        ),
                                      ],
                                    )),

                                // Image.asset("assets/logo.png"),
                              ],
                            ),
                          ],
                        )),
                  ),
                  InkWell(
                    child: Container(
                      margin: EdgeInsets.fromLTRB(20, 20, 20, 30),
                      height: 55,
                      // width: 200,
                      child: Card(
                        color: Colors.blueAccent,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                                // margin: EdgeInsets.fromLTRB(10, 0, 10, 0),

                                // width: 440,
                                ),

                            // Image.asset("assets/logo.png"),
                            new Text(
                              'Tab To Start a Course',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      // _register();
                    },
                  ),
                ])),
          ),
          // ),
        ));
  }
}
