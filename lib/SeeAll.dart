import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:meander_training/CaricullumScreen.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class SeeAll extends StatefulWidget {
  @override
  SeeAllPageState createState() => SeeAllPageState();
}

List users = [];
String _id;
String _creator_name;
String _course_name;
String _course_logo;
String overView;

class SeeAllPageState extends State<SeeAll> {
  @override
  void initState() {
    super.initState();
    getList();
  }

  // final scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  TextEditingController _password = TextEditingController();
  void getList() async {
    var response = await http.get(
      Uri.parse(
        "https://meander.training/api/homelisting",
      ),
    );
    var loginData = json.decode(response.body);
    setState(() {
      users = loginData['courses'];
    });
  }

  void _login() {}
  String validatePass(value) {
    print(value);
    if (value.isEmpty) {
      return "Requried";
    } else if (value.length < 4) {
      return "should Be atleast 4 charcaters";
    } else {
      return null;
    }
  }

  void search() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('user_id');
    var response = await http.get(
      Uri.parse(
        "https://meander.training/api/searchcourse?+ user_id=${token}&search=${_password.text}",
      ),
    );
    var loginData = json.decode(response.body);
    print(loginData);
    setState(() {
      users = loginData['search_course'];
    });
  }

  void SelectedCourse() {
    // if(_id==users[index]){

    // }
    // var data = json.decode(_id);
    //
    print(_id);
    // Navigator.of(context)
    //     .push(MaterialPageRoute(builder: (context) => CaricullumScreen(
    //       myObject: '',
    //     )));

    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => CaricullumScreen(
            text: _id,
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            // mainAxisAlignment: MainAxisAlignment.center,
            // crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset("assets/logo.png",
                  width: 90, height: 50, fit: BoxFit.fill),
              Text(
                "Meander Training",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    color: Colors.black),
              ),
            ],
          ),
          backgroundColor: Colors.white,
        ),
        body: Padding(
          padding: EdgeInsets.all(0),
          child: Center(
            child: Form(
              autovalidate: true,
              key: formkey,
              child: SizedBox.expand(
                // -> 01
                child: SingleChildScrollView(
                    child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,

                        children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(15),
                        child: TextField(
                          controller: _password,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0)),
                            labelText: 'Search course',
                            hintText: 'Search course',
                            suffixIcon: IconButton(
                              icon: Icon(
                                Icons.search,
                                color: Colors.blueAccent,
                              ),
                              onPressed: search,
                            ),
                          ),
                        ),
                      ),
                      RefreshIndicator(
                          onRefresh: _pullRefresh,
                          child: Container(
                            margin: EdgeInsets.fromLTRB(10, 30, 00, 0),
                            // height: 200.0,
                            child: ListView.builder(
                                padding: const EdgeInsets.all(8),
                                itemCount: users.length,
                                // scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                itemBuilder: (BuildContext context, int index) {
                                  return Container(
                                    alignment: Alignment.center,
                                    margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                                    child: InkWell(
                                        onTap: () {
                                          setState(() {
                                            _id =
                                                ' ${users[index]['course_id']}';
                                            _creator_name =
                                                ' ${users[index]['creator_name']}';
                                            _course_name =
                                                ' ${users[index]['course_name']}';
                                            _course_logo =
                                                ' ${users[index]['course_logo']}';
                                            overView =
                                                ' ${users[index]['overview']}';
                                          });
                                          SelectedCourse();
                                        },
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              // mainAxisAlignment: MainAxisAlignment.center,
                                              // crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                Image.asset("assets/book1.png",
                                                    width: 110,
                                                    height: 90,
                                                    fit: BoxFit.fill),
                                                Container(
                                                  width: 200,
                                                  margin: EdgeInsets.fromLTRB(
                                                      10, 10, 00, 0),
                                                  child: new Text(
                                                    ' ${users[index]['course_name']}',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 14,
                                                        color: Colors.black),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                )
                                              ],
                                            ),
                                            Text(
                                              '₹ 0',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18,
                                                  color: Colors.black),
                                            ),
                                          ],
                                        )),
                                  );
                                }),
                          )),
                    ])),
              ),
            ),
          ),
        ));
  }

  Future<void> _pullRefresh() async {
    getList();
    _password.text = '';
  }
}
